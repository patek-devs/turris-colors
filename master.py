import socket
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

iplist = ('192.168.1.1','192.168.1.211','192.168.1.109','192.168.1.178','192.168.1.244','192.168.1.179','192.168.1.196','192.168.1.133','192.168.1.217')


class ColourArray:
    def __init__(self, iplist=iplist):
        self.sockets = {}
        self.iplist = iplist
        # Create a TCP/IP socket
        for i in range(0, len(self.iplist)):
            self.sockets[i] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_address = (self.iplist[i], 10000)
            eprint('connecting to {} port {}'.format(server_address[0], server_address[1]))
            self.sockets[i].connect(server_address)

    def send(self, colorData):
        if len(self.sockets) != len(colorData):
            eprint("Error: invalid color data")
        for i in range(0,len(self.iplist)):
            self.sendTo(i,colorData[i])

    def sendTo(self, socketIndex, data):
        self.sockets[socketIndex].sendall(self.translateData(data))
        return

    def close(self):
        for i in range(0, len(self.sockets)):
            self.closeTo(i)
        return

    def closeTo(self, socketIndex):
        self.sockets[socketIndex].close()
        return

    def translateData(self, colorData):
        outputString = ""
        for i in colorData:
            outputString += "{} {} {}\n".format(i[0],i[1],i[2])
        return (outputString + "\n").encode("UTF-8")
