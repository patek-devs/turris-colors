from master import ColourArray
from time import sleep
import numpy

c = ColourArray(iplist=('192.168.1.1', ))

rainbow=[[[255,0,0],[255,127,0],[255,255,0],[127,255,0],[0,255,0],[0,255,127],[0,255,255],[0,127,255],[0,0,255],[255,0,255],[255,0,127],[255,0,0]]]

while True:
    arr = numpy.array(rainbow.copy()[0])
    for i in range(0,12):
        arr = numpy.roll(arr,1,axis=0)
        c.send([arr.tolist()])
        sleep(0.03)
