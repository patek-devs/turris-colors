from pwn import *
from master import ColourArray
s = {}
ips = ColourArray().iplist

for i in range(0, len(ips)):
    s[i] = ssh(host=ips[i],user='root',password='GNpvfm5%%')
    s[i].shell('/bin/bash', tty=True)
    s[i].run('echo 0 > /sys/class/leds/omnia-led:all/autonomous')
    s[i].run_to_end('python3 /root/slave.py & disown') # this isn't working properly, we ended up starting the script manually on each device
